#! /usr/bin/env bash

# docker-machine name
VM=${DOCKER_MACHINE_NAME:-default}

# set to non null to set a static ip
STATIC_IP=100

# Should babun-docker setup other volumes and which ones?
ADDITIONAL_DRIVES="d"
